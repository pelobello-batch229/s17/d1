console.log("Hello World")

// [SECTION] - Functions
// Function in JS are lines/blocks of codes that will tell device/application to perform a particula task.
// Function are mostly created to create complicated task to run several lines of codes succession.
// They are also used to prevent repeating line of codes.

//Function Declaration -> "function"
/*

Syntax:
function functionName(){
	//code block
	let number = 10;
}

conosole.log(number) -> error -> local scope variable

*/

function printGrade() {
	let grade1 = 90;
	let grade2 = 95;
	let ave = (grade1 + grade2)/2;
	console.log(ave);
	//console.log("My name is John");
}

// calling or invoke/invocation
printGrade();


// [SECTION] Function Invocation
// Invocation will execute block of code inside the function being called

// This will call function named printgrade
printGrade();

//sampleInvocation(); -> undeclared function cannot be inovoke.


// [SECTION] Function Declaration vs Expression

//This is a sample funtion declaration
function declaredFunction(){
	console.log("Hello World from declaredFunction()");
}
declaredFunction();

// This is a function expression
// A function can also b stored in a variable
// Anonymous function 0 a function without a name.

//variableFunction();

/*

	error 0 function expressions being stored in a let or const, cannot be hoisted

*/

// Anonymous function
// Function Expression Example
let	vairableFunction = function(){
	console.log("Hello Again!");
}

vairableFunction();

let functionExpression = function funcName(){
	console.log("Hello from the other side");
}

functionExpression(); //This is the right invocation in function expression
//funcName(); --> This will return error

// You can re-assign declared function and function expression to a new anonymous funtion

declaredFunction = function(){
	console.log("updated declaredFunction()");
}

declaredFunction();

functionExpression = function () {
	console.log("updated functionExpression()");
}

functionExpression();

// However, we cannot re-assign a function expression initialized with const.

const constantFunction = function(){
	console.log("initialized with const!");
}
constantFunction();

/*
constantFunction = function(){
	console.log("Will try to re-assign") --> error
}
constantFunction();
*/

// [SECTION] - Function Scoping
/*
1. Local/block Scope
2. Global Scope
3. Function Scope
*/

{
// This is a local Variable and its value is only accessible inside the curly braces.
	let	localVairable = "Armando Perez";
	console.log(localVairable);
}

// This is a global variable and its value is accessible anywhere in the code base
let globalVariable = "Mr. WorldWide";

console.log(globalVariable);
//console.log(localVairable); -> will return error

function showNames() {
	// Funcation scoped variables
	var functioVar = "Joe";
	const functionConst = "John";
	let functionlet = "Jane";

	console.log(functioVar);
	console.log(functionConst);
	console.log(functionlet);
}

showNames();

/*console.log(functioVar);
console.log(functionConst);
console.log(functionlet);

This will return error sice the variables are stored in a function
*/


// Nested Function

function myNewFunction(){
	// global variable inside of a function
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		//child function can inherit data from parent function
		console.log(nestedName);
		console.log(name);
	}
	nestedFunction();

}
myNewFunction();
//nestedFunction(); --> result will be error

// Function and Global SCope Variables

let globalName = "Alexandro";

function mynewFunction2(){
	let nameInside = "Renz";

	console.log(globalName);
}

mynewFunction2();
//console.log(nameInside); --> will return an error

// [SECTION] - Using Alert
// alert() allows us to show small window at the top of  our browser.

//alert("Hello World");

function showSampleAlert(){
	alert("Show User!");
}

showSampleAlert();
console.log("I will only log in the console when alert is dismissed");

//Notes on the use of alert()
// Show only alert() for short dialog message.
// Do not overuse alert() because program/js has to wait for it to be dismissed befor continuing.

// [SECTION] - Using prompt()
// promp()- allows us to show a small window and gather user input.

let samplePrompt = prompt("Enter your Name.");

//console.log("Hello, " + samplePrompt);
//console.log(typeof samplePrompt);

let sampleNullPrompt = prompt("Do not enter anything.");
console.log(sampleNullPrompt); // return an empty string

function printWelcomeMessage(){
	let firstName = prompt("Enter your first Name.");
	let lastName = prompt("Enter your last Name.");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}
printWelcomeMessage();

// [SECTION] - Function Nameing Convention
// Function Names should be definitive of the task it wil perform. It usually contains verb.

function getCourse(){
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}

getCourse();

/// Avoid generic names to avoid confusion within your code.

/*function get(){
	let name = "Jamie";
	console.log(name);
}
get();*/